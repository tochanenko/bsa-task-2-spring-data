package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("select o from User u inner join u.office o inner join u.team t where t.technology.name = :technology")
    List<Office> findAllByTechnology(String technology);

    Office findByAddress(String address);
}
