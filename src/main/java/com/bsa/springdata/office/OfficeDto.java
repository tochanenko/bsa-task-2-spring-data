package com.bsa.springdata.office;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@Builder
public class OfficeDto {
    private final UUID id;
    private final String city;
    private final String address;

    public static OfficeDto fromEntity(Office office) {
        return OfficeDto
                .builder()
                .id(office.getId())
                .address(office.getAddress())
                .city(office.getCity())
                .build();
    }

    public static List<OfficeDto> fromEntityList(List<Office> offices) {
        return offices
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());
    }
}
