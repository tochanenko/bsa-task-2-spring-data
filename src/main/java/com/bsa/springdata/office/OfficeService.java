package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        return OfficeDto.fromEntityList(
                officeRepository.findAllByTechnology(technology)
        );
        // DONE: Use single query to get data.
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {

        // TODO: Remake it!

        Office office = officeRepository.getOne(officeRepository.findByAddress(oldAddress).getId());
        office.setAddress(newAddress);
        officeRepository.save(office);

        return Optional.of(OfficeDto.fromEntity(office));

        // DONE: Use single method to update address. In order to get the new office you can make extra query
        //  Hint: Every user is connected to one of the project. There cannot be any users without a project.
    }
}
