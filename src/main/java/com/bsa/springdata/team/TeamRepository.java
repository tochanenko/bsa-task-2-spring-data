package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String name);

    @Query("select count(t) from Team t where t.technology.name = :technology")
    int countByTechnologyName(String technology);

    @Query(
            value = "select * from teams t inner join users u on t.id = u.team_id group by t.id, u.id having count(u.team_id) < :number limit 1",
            nativeQuery = true
    )
    Team getWithLessNumberOfUsers(int number);
}
