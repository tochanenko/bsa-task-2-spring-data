package com.bsa.springdata.team;

import com.bsa.springdata.team.dto.TeamDto;
import com.bsa.springdata.team.dto.TechnologyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        Team team = teamRepository.getWithLessNumberOfUsers(devsNumber);
        Optional<Technology> technology = technologyRepository.findByName(newTechnologyName);

        if (technology.isEmpty()) {
            throw new RuntimeException("It should not happen!");
        }

        team.setTechnology(technology.get());
        teamRepository.save(team);

        // DONE: You can use several queries here. Try to keep it as simple as possible
    }

    public void normalizeName(String hipsters) {
        // TODO: Use a single query. You need to create a native query
//        teamRepository.normalizeName(hipsters);
    }
}
