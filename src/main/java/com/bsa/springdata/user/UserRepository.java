package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    @Query("select u from User u where u.office.city = :city order by u.lastName asc")
    List<User> findAllByCity(String city);

    List<User> findAllByLastName(String lastName, /*Sort sort,*/ Pageable pageable);

    List<User> findAllByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    // TODO: Use Sort class to sort results instead of using "order by"
    @Query("select u from User u where u.office.city = :city and u.team.room = :room order by u.lastName asc")
    List<User> findAllByRoomAndCity(String room, String city, Sort sort);

    @Modifying
    @Query("delete from User u where u.experience < :experience")
    int deleteByExperience(int experience);
}
