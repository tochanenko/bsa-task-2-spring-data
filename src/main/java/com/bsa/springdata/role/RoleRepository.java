package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Transactional
    @Modifying
    @Query(
            value = "delete from roles r where r.code = :roleCode and r.id not in (select u2r.role_id from users u inner join user2role u2r on u.id = u2r.user_id)",
            nativeQuery = true
    )
//    @Query("delete from Role r where r.code = :roleCode and r.id not in (select u.id from User.roles u)")
    int deleteAllByNameWithoutUsers(String roleCode);
}
