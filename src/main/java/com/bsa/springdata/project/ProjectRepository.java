package com.bsa.springdata.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
//    @Query("select p from Project p inner join Team.project t inner join Team.technology tech where (select count(u) from User u where u.team.project = p.id)")
//    List<Project> findTop5ByTechnology(String technology);

//    @Query("select count(distinct p) from Project p inner join Team.project t inner join User.team u where u.technology = :technology")
//    int countByRole(String role);
}