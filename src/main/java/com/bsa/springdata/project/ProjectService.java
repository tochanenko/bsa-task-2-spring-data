package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return null;
        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
    }

    public Optional<ProjectDto> findTheBiggest() {
        return Optional.empty();
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
    }

    public List<ProjectSummaryDto> getSummary() {
        return null;
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
    }

    public int getCountWithRole(String role) {
//        return projectRepository.countByRole(role);
        return 0;
        // DONE: Use a single query
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Team");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Project project = new Project();
        project.setName(createProjectRequest.getProjectName());
        project.setDescription(createProjectRequest.getProjectDescription());

        Technology technology = new Technology();
        technology.setName(createProjectRequest.getTech());
        technology.setDescription(createProjectRequest.getTechDescription());
        technology.setLink(createProjectRequest.getTechLink());

        Team team = new Team();
        team.setName(createProjectRequest.getTeamName());
        team.setArea(createProjectRequest.getTeamArea());
        team.setProject(project);
        team.setTechnology(technology);
        team.setRoom(createProjectRequest.getTeamRoom());

        em.persist(project);
        em.persist(technology);
        em.persist(team);

        em.getTransaction().commit();

        em.close();
        emf.close();

        return team.getId();
        // DONE: Use common JPARepository methods. Build entities in memory and then persist them
    }
}
